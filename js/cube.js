(function (Cube, $, undefined) {
    'use strict';
    var props = ['transform', 'WebkitTransform', 'MozTransform', 'OTransform', 'msTransform'],
        prop,
        el = document.createElement('div'),
        i,
        l,
        x,
        y,
        xAngle = 0,
        yAngle = 0;

    for (i = 0, l = props.length; i < l; i++) {
        if (typeof el.style[props[i]] !== "undefined") {
            prop = props[i];
            break;
        }
    }

    

    var supportPreseve3D = true;
    //we only need to call this once for preserve3d feature detection
    var getPerspective = function () {
        var element = document.createElement('p'),
            i,
            html = document.getElementsByTagName('HTML')[0],
            body = document.getElementsByTagName('BODY')[0],
            propertyList = {
                'webkitTransformStyle' : '-webkit-transform-style',
                'MozTransformStyle' : '-moz-transform-style',
                'msTransformStyle' : '-ms-transform-style',
                'transformStyle' : 'transform-style'
            };
        
        body.insertBefore(element, null);
        
        
        for (i in propertyList) {
            if (propertyList.hasOwnProperty(i)) {
                if (element.style[i] !== undefined) {
                    element.style[i] = "preserve-3d";
                }
            }
        }

        var st = window.getComputedStyle(element, null),
            transform = st.getPropertyValue("-webkit-transform-style") ||
                        st.getPropertyValue("-moz-transform-style") ||
                        st.getPropertyValue("-ms-transform-style") ||
                        st.getPropertyValue("transform-style");

        if (transform !== 'preserve-3d') {
            supportPreseve3D = false;
        }
        document.body.removeChild(element);

    };
    
    getPerspective();
    
    //add fall back css for IE as preserve3d doesn't work
    if (!supportPreseve3D) {
        
        $('.face').addClass("face-ie");
        $('.one').addClass("animate1");
        $('.two').addClass("animate2");
        $('.three').addClass("animate3");
        $('.four').addClass("animate4");
        $('.five').addClass("animate5");
        $('.six').addClass("animate6");
    } else {
        $('.face').addClass("face-not-ie");
    }
    
    $(document).keydown(function (evt) {
        
        if (!supportPreseve3D) {
            return;
        }
        
        switch (evt.keyCode) {
        case 37: // left
            yAngle -= 90;
            break;

        case 38: // up
            xAngle += 90;
            evt.preventDefault();
            break;

        case 39: // right
            yAngle += 90;
            break;

        case 40: // down
            xAngle -= 90;
            evt.preventDefault();
            break;
        }
        document.getElementById('cube').style[prop] = "rotateX(" + xAngle + "deg) rotateY(" + yAngle + "deg)";
    });
    
    $(".camera-view").bind('mousedown', function (e) {

        var target = e.currentTarget;
        //if ($(e.target).hasClass('face') || $(e.target).hasClass('camera-view')) {
        if ($(e.currentTarget).hasClass('camera-view')) {
            console.log(e);
           
            $('.face').addClass("face_animate_color");
            
            //do not attempt to do anymore if IE
            if (!supportPreseve3D) {
                return;
            }
            
            var leftOffSet = parseInt($(this).css('margin-left').replace("px", ""), 10) + parseInt($(this).css('padding-left').replace("px", ""), 10);
            var topOffSet = parseInt($(this).css('margin-top').replace("px", ""), 10); // + parseInt($(this).css('padding-top').replace("px", ""));
            
            if (e.type === "mousedown") {
                x =  e.pageX - ($(this).position().left) - leftOffSet;
                y = e.pageY -  ($(this).position().top) - $('header').height() - topOffSet;
            }
            if (e.type === "touchstart") {
                x = e.originalEvent.touches[0].pageX -  leftOffSet;
                y = e.originalEvent.touches[0].pageY - ($(this).position().top) - $('header').height();
            }
        
            console.log("x: " + x + " y: " + y);
            console.log("topOffSet: " + topOffSet);
            var minWidth = 0;
            var maxWidth = $(target).height();

            var minHeight = 0;
            var maxHeight = $(target).width();

            var widthLowerThird = ($(target).width() / 3);
            var widthHigherThird = ($(target).width() / 3) * 2;

            var heightLowerThird = ($(target).height() / 3);
            var heightHigherThird = ($(target).height() / 3) * 2;

            if (x < widthLowerThird) {
                console.log("cube left hit");
                yAngle -= 90;
            }

            if (x > widthHigherThird) {
                console.log("cube right hit");
                yAngle += 90;
            }

            if (y < heightLowerThird) {
                console.log("cube top hit");
                xAngle += 90;
            }

            if (y > heightHigherThird) {
                console.log("cube bottom hit");
                xAngle -= 90;
            }
            document.getElementById('cube').style[prop] = "rotateX(" + xAngle + "deg) rotateY(" + yAngle + "deg)";
        }
    });

    var removeFaceAnimation = function () {
        var faceElements = document.getElementsByClassName("face");
        for (i = 0; i < faceElements.length; i++) {
            $(faceElements[i]).removeClass("face_animate_color");
        }
    };

    $(document).bind('mouseup touchend', function (evt) {
        removeFaceAnimation();
    });

    $(document).on('mouseout', function (evt) {
        if ($(evt.target).hasClass('face') || $(evt.target).hasClass('camera_view')) {
            removeFaceAnimation();
        }
    });
                    
    
}(window.Cube = window.Cube || {}, jQuery));