(function (MainApp, $, undefined) {
    'use strict';
    
    //IE 11 and below doesnt support web audio api or preserve3d in css animations 
    //we need a function that tells us if we are using IE.
    var selectedMenuID,
        selectedTopicID = 0,
        scrollPosition = 0,
        showMenuList = false,
        titleVisible = true,
        topicLocations = [],
        autoScroll = false,
        timer;
    
    MainApp.scrollPosition = function () {
        return scrollPosition;
    };
    
    
    var detectDiv = document.createElement("div");
    
    
    detectDiv.style.transformStyle = "preserve-3d";
    if (detectDiv.style.transformStyle.length > 0) {
        document.getElementsByTagName("html")[0].className = "preserve3D";
    }

    //buttons flash
    var initButton = function (button) {
        console.log("initButton");
        console.log("initButton");
        setTimeout(function () {
            button.addClass("menu-select");
        }, 0);
        setTimeout(function () {
            button.addClass("menu-deselect").removeClass("menu-select");
        }, 500);
    };
    
    //top menu for desktop displays
    var highlightSelectedMenuButton = function () {
        console.log("highlightSelectedMenuButton topicID=" + selectedTopicID);
        setTimeout(function () {
            var relaventMenuButton = $(".menu-button[data-topicID=" + selectedTopicID + "]");
            $(relaventMenuButton).siblings('.menu-button').each(function (index) {
                //we don't want to animate de-select if the menu button hasn't already been seleced or it will flash
                if ($(this).hasClass("menu-select")) {
                    $(this).addClass("menu-deselect").removeClass("menu-select");
                }
            });
            $(relaventMenuButton).addClass("menu-select").removeClass("menu-deselect");
        }, 0);
    };
    
    //menu for small displays
    var highlightSelectedMenuListButton = function () {
        console.log("highlightSelectedMenuListButton");
        var relaventMenuListButton = $(".menu-list-item-button[data-topicID=" + selectedTopicID + "]");
        $(relaventMenuListButton).siblings('.menu-list-item-button').each(function (index) {
            if ($(this).hasClass("menu-list-item-button-selected")) {
                $(this).removeClass("menu-list-item-button-selected");
            }
        });
        if ($(relaventMenuListButton).hasClass("menu-list-item-button-selected") === false) {
            $(relaventMenuListButton).addClass("menu-list-item-button-selected");
        }
        
    };
    
    //Once selectedTopicID has been set we can scroll to the first ariticle with that topicID
    var scrollToAricle = function () {
        console.log("scrollToAricle: " + selectedTopicID);
        if (!selectedTopicID) {
            return;
        }
        var firstRelevantArticle = $("article[data-topicID=" + selectedTopicID + "]");
        if (firstRelevantArticle) {
            var firstRelevantArticleHeight = firstRelevantArticle.position().top;
            var calculatedHeight = $('main').scrollTop() + firstRelevantArticleHeight;
            $('main').animate({ scrollTop: calculatedHeight + "px" });
            console.log("firstRelevantArticleHeight: " + firstRelevantArticleHeight);
        } else {
            console.log("no firstRelevantArticle");
        }
        
        setTimeout(function () {
            autoScroll = false;
        }, 500);
    
    };
    
    var setTopicID = function (topicID) {
        selectedTopicID = topicID;
        console.log("selectedTopicID: " + selectedTopicID);
        highlightSelectedMenuButton();
        highlightSelectedMenuListButton();
        autoScroll = true;
        scrollToAricle();
    };
    
    var toggleShowMenuList = function (e) {
        console.log("toggleShowMenuList");
        showMenuList = !showMenuList;

        if (showMenuList) {
            console.log("Show MenuList");
            $('#menu-list-button').addClass("menu-select").removeClass("menu-deselect");
            $('#menu-list').addClass("menu-list-expand").removeClass("menu-list-contract");
            
        } else {
            $('#menu-list-button').addClass("menu-deselect").removeClass("menu-select");
            //stop contract animation on load
            if ($('#menu-list').height() > 0) {
                console.log("Hide MenuList");
                $('#menu-list').addClass("menu-list-contract").removeClass("menu-list-expand");
            }
        }
    };
    
    var initMenuButtons = function () {
        console.log("initMenuButtons");
        $($('.menu-button').get().reverse()).each(function (i) {
            var menuButton = $(this);
            setTimeout(function () {
//                if (i === $('.menu-button').length - 1) {
//                    //as we are doing this in reverse the last button to flash is the first topic so select it
//                    highlightSelectedMenuButton(menuButton);
//                } else {
                initButton(menuButton);
                
            }, 200 * (i + 1));
            
        });
        
        initButton($('#menu-list-button'));
    };
    
    var initListMenu = function () {
        $('#menu-list-group').addClass("menu-list-group-lower");
        //$('#menu-list').addClass("menu-list-contract");
    };
    
    $('.menu-button').click(function (e) {
        console.log("menu-button click");
        setTopicID($(e.currentTarget).attr("data-topicID"));
    });
    
    $('#menu-list-button').click(function (e) {
        console.log("menu-list-button click");
        toggleShowMenuList();
    });
    
    $('.menu-list-item-button').click(function (e) {
        console.log("menu-list-item-button click");
        setTopicID($(e.currentTarget).attr("data-topicID"));
        toggleShowMenuList(e);
    });
    
    var hideTitle = function () {
        console.log("hideTitle");
       
        setTimeout(function () {
            $('#menu-list-group').addClass("menu-list-group-higher").removeClass("menu-list-group-lower");
            $('#title').addClass("title-hide").removeClass("title-show");
            $('header').addClass("header-shrink").removeClass("header-grow");
            $('main').addClass("main-expand").removeClass("main-contract");
        }, 0);
        titleVisible = false;
    };
    
    var showTitle = function () {
        console.log("showTitle");
        setTimeout(function () {
            $('#menu-list-group').addClass("menu-list-group-lower").removeClass("menu-list-group-higher");
            $('#title').addClass("title-show").removeClass("title-hide");
            $('header').addClass("header-grow").removeClass("header-shrink");
            $('main').addClass("main-contract").removeClass("main-expand");
        }, 0);
        titleVisible = true;
    };
    
    //highlight relevent topic in the menu based on scroll position
    var setSelectedTopicByY = function (scrollPosition) {
        console.log("setSelectedTopicByY y=" + scrollPosition);
        var i = 0;
        for (i = 1; i < topicLocations.length; i++) {
            if (topicLocations[i] >= scrollPosition) {
                if (i > 1) {
                    selectedTopicID = i - 1;
                    break;
                } else {
                    selectedTopicID = 1;
                    break;
                }
            }
        }
        
        
        // now we want to select the last menu item when we are near the bottom 
        // as we will never scroll past the last article even on maximum scroll
        
        var scrollPositionWithHeight = scrollPosition + $('main').height();
        var theBottom = $('#grid').outerHeight() - 30;
        
        if (scrollPositionWithHeight > theBottom) {
            console.log("At bottom");
            selectedTopicID = i;
        } else {
            console.log("scrollPositionWithHeight = " + scrollPositionWithHeight);
            console.log("theBottom = " + theBottom);
        }
        
        //now we have found the selected topic update the selected menu items
        
        highlightSelectedMenuButton();
        highlightSelectedMenuListButton();
    };
    
    $('main').scroll(function () {
        if (timer) {
            window.clearTimeout(timer);
        }
        timer = window.setTimeout(function () {
            // actual callback
            scrollPosition = $('main').scrollTop();
            if (scrollPosition > 0) {
                hideTitle();
            } else {
                showTitle();
            }
            setSelectedTopicByY(scrollPosition);
        }, 100);
    });
    
    
    
    //find the top most article for each topic and list its Y postion 
    var setTopicLocations = function () {
        console.log("setTopicLocations");
        
        topicLocations = [];
        
        $('article').each(function () {
            var articleTopicID =  $(this).attr("data-topicID");
            var articleY = $(this).position().top;
            console.log("articleTopicID = " + articleTopicID + "  articleY = " + articleY);
            if (!topicLocations[articleTopicID] || topicLocations[articleTopicID] > articleY) {
                console.log("TOPMOST = articleTopicID = " + articleTopicID + "  articleY = " + articleY);
                topicLocations[articleTopicID] = articleY;
            }
        });
    };
    
    $(document).ready(function () {
        initMenuButtons();
        initListMenu();
        setTopicLocations();
    });

}(window.MainApp = window.MainApp || {}, jQuery));