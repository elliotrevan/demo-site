(function (Theremin, $, undefined) {
    'use strict';
    var browserWebAudioAPICompatible = false,
        audioContext,
        oscillator,
        gainNode,
        maxFreq = 2000,
        maxGain = 0.5,
        freq = 440,
        gain = 1,
        currentX,
        currentY,
        previousX,
        previousY,
        active = false,
        oscRunning = false,
        message = "Touch";
    
    
    
    
    var drawInstructions = function () {
        console.log("drawInstructions");
        var canvas = document.getElementById("theremin");
        var ctx = canvas.getContext("2d");
        ctx.font = "30px Quicksand";
        ctx.fillStyle = '#444';
        ctx.fillText(message, 55, 110, 200);
    };
    
    var clearCanvas = function () {
        console.log("clearCanvas");
        
        var canvas = document.getElementById("theremin");
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    };
    
    function init() {
        try {
            if (window.AudioContext) {
                audioContext = new AudioContext();
            } else {
                audioContext = new WebkitAudioContext();
            }
            browserWebAudioAPICompatible = true;
            drawInstructions();
            
		} catch (e) {
            browserWebAudioAPICompatible = false;
            console.log("web audio not supported");
            drawInstructions();
        }
    }
    
    
    window.addEventListener('load', init, false);

    ///////////////////////////////////////  IE fall back because it can't do web audio API *grumble* /////////////////////////////////////////////
    //Getting hold of swf with IE fix
    var theProduct = function (productName) {
        if (navigator.appName.indexOf("Microsoft") !== -1) {
            return window[productName];
        } else {
            return document[productName];
        }
    };
  
    var flashPlay = function () {
        console.log("flash play");
        //console.log("Flash play");  
        theProduct("sawWave").play();
    };
    
    var flashStop = function () {
        //console.log("Flash stop");  
        theProduct("sawWave").stop();
    };
    
    //send sync server messages to the product
    var flashSetFrequency = function (value) {
        //console.log("Flash set frequency", value);
        theProduct("sawWave").frequency(value);
    };
    
    //send sync server messages to the product
    var flashSetGain = function (value) {
        //console.log("Flash set gain", value);
        theProduct("sawWave").gain(value);
    };
    ///////////////////////////////////////////////////////// web audio API -the good stuff ////////////////////////////////////////////
    
    var buildOsc = function () {
        console.log("build osc");
        oscillator = audioContext.createOscillator();
        oscillator.type = 'saw';

        gainNode = audioContext.createGain();
        oscillator.connect(gainNode);
        gainNode.connect(audioContext.destination);
    };
    
    var playOsc = function () {
        oscillator.start(0);
        oscRunning = true;
    };
    
    var stopOsc = function () {
        if (!oscRunning) {
            return;
        }
        //stop clipping audio half way through wave form
        while (gainNode.gain.value > 0) {
            gainNode.gain.value = gainNode.gain.value - 0.00001;
        }

        if (gainNode.gain.value <= 0) {
            oscillator.stop();
            oscRunning = false;
        }
    };
    
    
    ////////////////////////////////////////////////// event listeners /////////////////////////////////////////
    
    $('#theremin').bind('mousedown touchstart', function (e) {
        
        e.preventDefault();
        active = true;
        
        if (e.type === "mousedown") {
            currentX = e.pageX - e.target.offsetLeft;
            currentY = (e.pageY + MainApp.scrollPosition() - $('header').height()) - e.target.offsetTop;
        }
        if (e.type === "touchstart") {
            currentX =  (e.originalEvent.touches[0].pageX) -  e.originalEvent.touches[0].target.offsetLeft;
            currentY = (e.originalEvent.touches[0].pageY + MainApp.scrollPosition() - $('header').height()) - e.originalEvent.touches[0].target.offsetTop;
            
//            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
//            var pageX = touch.pageX;
//            var pageY = touch.pageY;
//            var offsetLeft = touch.target.offsetLeft;
//            var offsetTop = touch.target.offsetTop;
//            
//            console.log("pageX: "+pageX);
//            console.log("pageY: "+pageY);
//            console.log("offsetLeft: "+offsetLeft);
//            console.log("offsetTop: "+offsetTop);
        }
        
        //must not be negative
        if (currentX < 0 || currentY < 0) {
            return;
        }
        
        freq = currentX / e.target.width * maxFreq;
        gain = currentY / e.target.height * maxGain;
        
        if (!browserWebAudioAPICompatible) {
            flashSetFrequency({value: freq});
            flashSetGain({value: gain});
            flashPlay();
        } else {
            buildOsc();
            
            oscillator.frequency.value = freq;
            gainNode.gain.value = gain;

            playOsc();
        }
    });
    
    $(window).bind('mouseup touchend', function (evt) {
        active = false;
        if (!browserWebAudioAPICompatible) {
            flashStop();
        } else {
            stopOsc();
        }
        clearCanvas();
        drawInstructions();
    });
    
    $('#theremin').bind('mouseout touchcancel', function (evt) {
        
        if (!browserWebAudioAPICompatible || !oscRunning) {
            return;
        }
        
        //oscillator.stop(); 
        //oscRunning = false;
    });
    

    
    var drawCircles = function (x, y) {
        if (x < 0 || y < 0) {
            return;
        }
        
        var canvas = document.getElementById("theremin");
        var ctx = canvas.getContext("2d");
        ctx.beginPath();
        var radius = 100 * gain;
        ctx.arc(x, y, radius, 0, 2 * Math.PI);
        ctx.lineWidth = 5;
        var colour = $.xcolor.gradientlevel('#255459', '#E374FA', x, canvas.width);
        if (colour) {
            ctx.strokeStyle = colour.getHex();
        }
        ctx.stroke();
        
        ctx.globalAlpha = 0.1;
        ctx.rect(0, 0, ctx.canvas.width, ctx.canvas.height);
        ctx.fillStyle = "#CCCC4F";
        ctx.fill();
        ctx.globalAlpha = 1;
    };
    
    // TODO not working with touch move
    
    $('#theremin').bind('mousemove touchmove', function (e) {
        
        if (!active) {
            return;
        }

        if (e.type === "mousemove") {
            currentX = e.pageX - e.target.offsetLeft;
            currentY = (e.pageY + MainApp.scrollPosition() - $('header').height()) - e.target.offsetTop;
        }
        if (e.type === "touchmove") {
            currentX =  (e.originalEvent.touches[0].pageX) -  e.originalEvent.touches[0].target.offsetLeft;
            currentY = (e.originalEvent.touches[0].pageY + MainApp.scrollPosition() - $('header').height()) - e.originalEvent.touches[0].target.offsetTop;
        }
        
          //must not be negative
        if (currentX < 0 || currentY < 0) {
            return;
        }
        
        freq = currentX / e.target.width * maxFreq;
        gain = currentY / e.target.height * maxGain;
    
        drawCircles(currentX, currentY);
      
        previousX = currentX;
        previousY = currentY;
        
        if (!browserWebAudioAPICompatible) {
            flashSetFrequency({value: freq});
            flashSetGain({value: gain});
        } else {
            if (!oscRunning) {
                return;
            }
            oscillator.frequency.value = freq;
            gainNode.gain.value = gain;
        }
        
    });
    
    $('#theremin').on('doubleclick', function (evt) {
        // TODO: stop text select on quick mouse clicks
        evt.preventDefault();
    });

    
    
}(window.Theremin = window.Theremin || {}, jQuery));