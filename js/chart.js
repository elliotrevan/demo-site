(function (Chart, $, undefined) {
    'use strict';
    var margin = {top: 20, bottom: 30},
        width = 200,
        height = 140 - margin.bottom,
        x = d3.scale.ordinal().rangeRoundBands([0, width], 0.1),
        y = d3.scale.linear().range([(height + margin.bottom), 0]),
        xAxis = d3.svg.axis().scale(x).orient("bottom");
    
    var chart = d3.select("#chart")
        .attr("width", width)
        .attr("height", height + margin.bottom);

    
    var _data;
    
    Chart.data = function () {
        return _data;
    };
        
    d3.csv("data/scores.csv", type, function(error, data) {   
        if (error) {
            return console.error(error);
        } else { 
            _data = data;
            drawChart('low');
        }
    });
    
    function type(d) {
        d.value = +d.value; // coerce to number
        return d;
    }
    
    var drawChart = function(field) { 
        
        x.domain(_data.map(function (d) { return d.name; }));
        y.domain([0, d3.max(_data, function (d) { return d[field]; })]);
        
        chart.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + (height + 1) + ")")
        .call(xAxis);
        
        var barWidth = width / _data.length -1;
        
        var  rects = chart.selectAll("rect")
            .attr("y", height)
            .attr("height", 0);
        
        //get all bars
        var  bars = chart.selectAll(".bar");
        //remove all old bars
        bars.remove();
        //set data
        bars = chart.selectAll(".bar").data(_data);
        //add svg
        bars.enter().append("g")
            .attr("class", "bar")
            .attr("transform", function (d) { return "translate(" + x(d.name) + ",0)"; });
        //add shape
        bars.append("rect")
            .attr("width", x.rangeBand())
            .attr("y", height)
            .attr("height", 0)
            .transition().delay(function (d, i){ return i * 150; })
            .duration(200)
            .attr("y", function (d) { return y(d[field]); })
            .attr("height", function (d) { return height - y(d[field]); });

        //add label
        bars.append("text")
            .attr("class", "text")
            .attr("x", x.rangeBand() / 2)
            .attr("y", height)
            .attr("dy", "0em")
            .transition().delay(function (d, i) { return i * 150; })
            .duration(200)
            .attr("y", function (d) { return y(d[field]) + 3; })
            .attr("dy", ".75em")
            .text(function (d) { return d[field]; });
        // exit selection
        bars.exit();

    };
    
    var updateChart = function(field) {
        
        
        x.domain(_data.map(function (d) { return d.name; }));
        y.domain([0, d3.max(_data, function (d) { return d[field]; })]);
        //update shape
        chart.selectAll("rect")
            .data(_data)
            .transition().delay(function (d, i) { return i * 150; })
            .duration(200)
            .attr("y", function (d) { return y(d[field]); })
            .attr("height", function (d) { return height - y(d[field]); });

        //update label
        chart.selectAll(".text")
            .data(_data)
            .attr("opacity", 0)
            .transition().delay(function (d, i) { return i * 150; })
            .duration(200)
            .attr("opacity", 1)
            .attr("y", function (d) { return y(d[field]) + 3; })
            .attr("dy", ".75em")
            .text(function (d) { return d[field]; });
        // exit selection
     
    };
    
    
    $("input[name=chart-button]").change(function () {
        var chartButonFormID = $('#chart-buttons-form').id;
        var value = $('input[name=chart-button]:checked', '#chart-buttons-form').val();
        console.log(value);
        if (value) {
            updateChart(value);
        }
    });
    
//    $(".toggle-btn-grp").change(function() {
//        value = $('input[name=chart-button]:checked', '#chart-buttons-form').val();
//        console.log(value); 
//        if(value)
//        {
//            setData(value);
//        }
//    }); 
//    
//    $(".chart-button").select(function() {
//        console.log("select");
//        
//    });
    

    
}(window.Chart = window.Chart || {}, jQuery));