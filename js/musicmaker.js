(function (MusicMaker, $, undefined) {
    'use strict';
    var goActive = false,
        drumsActive = false,
        bassActive = false,
        leadActive = false,
        breakActive = false,
        drumsLoaded = false,
        bassLoaded = false,
        leadLoaded = false,
        breakLoaded = false,
        blobsMoving = false,
        loading = false,
        drumsSound = new buzz.sound("audio/beat", {loop: true, preload: false, webAudioApi: true, formats: ["wav", "mp3"]}),
        bassSound = new buzz.sound("audio/bass", {loop: true, preload: false, webAudioApi: true, formats: ["wav", "mp3"]}),
        leadSound = new buzz.sound("audio/lead", {loop: true, preload: false, webAudioApi: true, formats: ["wav", "mp3"]}),
        breakSound = new buzz.sound("audio/break", {loop: true, preload: false, webAudioApi: true, formats: ["wav", "mp3"]});
  
    // not happy about using wavs but ogg and mp3 either have gap or go out of time with each other.
//    var drumsSound = new buzz.sound("https://dl.dropboxusercontent.com/u/886957/Audio/beat.wav", {loop:true, preload: false, webAudioApi:true}),
//    bassSound = new buzz.sound("https://dl.dropboxusercontent.com/u/886957/Audio/bass.wav", {loop:true, preload: false, webAudioApi:true}),
//    leadSound = new buzz.sound("https://dl.dropboxusercontent.com/u/886957/Audio/lead.wav", {loop:true, preload: false, webAudioApi:true}),
//    breakSound = new buzz.sound("https://dl.dropboxusercontent.com/u/886957/Audio/break.wav", {loop:true, preload: false, webAudioApi:true});
    
    var allSoundsInactive = function () {
        var allOff = false;
        if (!drumsActive && !bassActive && !leadActive && !breakActive) {
            allOff = true;
        }
        return allOff;
    };
    
    var allSoundsLoaded = function () {
        var allLoaded = false;
        if (drumsLoaded && bassLoaded && leadLoaded && breakLoaded) {
            allLoaded = true;
            console.log("all loaded");
        }
        return allLoaded;
    };
    var allSoundsNotLoaded = function () {
        var allNotLoaded = false;
        if (!drumsLoaded && !bassLoaded && !leadLoaded && !breakLoaded) {
            allNotLoaded = true;
            console.log("not all loaded");
        }
        return allNotLoaded;
    };
    
    var expandBlobs = function () {
        blobsMoving = true;
        setTimeout(function () {
            $('#go-container').removeClass("blob-centre-in").addClass('blob-centre-out');
            $('#drums-container').removeClass("blob-left-top-in").addClass('blob-left-top-out');
            $('#bass-container').removeClass("blob-right-top-in").addClass('blob-right-top-out');
            $('#lead-container').removeClass("blob-left-bottom-in").addClass('blob-left-bottom-out');
            $('#break-container').removeClass("blob-right-bottom-in").addClass('blob-right-bottom-out');
        }, 0);
        setTimeout(function () {
            $('#go-btn').html("Stop");
            blobsMoving = false;
        }, 2100);
    };
    
    // unmute active instruments
    var setInstruments = function () {
        if (drumsActive) {
            drumsSound.unmute();
        } else {
            drumsSound.mute();
        }
        
        if (bassActive) {
            bassSound.unmute();
        } else {
            bassSound.mute();
        }
        
        if (leadActive) {
            leadSound.unmute();
        } else {
            leadSound.mute();
        }
      
        if (breakActive) {
            breakSound.unmute();
        } else {
            breakSound.mute();
        }
    };
    
    //we want all instruments to pulse at the same time so we will trigger them all in the same function
    var setInstrumentPulse = function () {
        setTimeout(function () {
            $('#drums-btn').removeClass('pulse-anim');
            $('#bass-btn').removeClass('pulse-anim');
            $('#lead-btn').removeClass('pulse-anim');
            $('#break-btn').removeClass('pulse-anim');
            
            if (drumsActive) {
                $('#drums-btn').addClass('pulse-anim');
            }
            if (bassActive) {
                $('#bass-btn').addClass('pulse-anim');
            }
            if (leadActive) {
                $('#lead-btn').addClass('pulse-anim');
            }
            if (breakActive) {
                $('#break-btn').addClass('pulse-anim');
            }
        }, 0);
    };
    
    var stopAllInstruments = function () {
        drumsActive = false;
        bassActive = false;
        leadActive = false;
        breakActive = false;
        setInstrumentPulse();
        buzz.all().fadeOut(1800, setInstruments);
    };
    
    var contractBlobs = function () {
        blobsMoving = true;
        stopAllInstruments();
        setTimeout(function () {
            $('#go-container').removeClass("blob-centre-out").addClass('blob-centre-in');
            $('#drums-container').removeClass("blob-left-top-out").addClass('blob-left-top-in');
            $('#bass-container').removeClass("blob-right-top-out").addClass('blob-right-top-in');
            $('#lead-container').removeClass("blob-left-bottom-out").addClass('blob-left-bottom-in');
            $('#break-container').removeClass("blob-right-bottom-out").addClass('blob-right-bottom-in');
        }, 0);
        setTimeout(function () {
            $('#go-btn').html("Go");
            blobsMoving = false;
        }, 2100);
    };
    
    var checkLoaded = function () {
        var allLoaded = allSoundsLoaded();
        var allNotLoaded = allSoundsNotLoaded();
        if (allLoaded && goActive) {
            console.log("check loaded all loaded");
            loading = false;
            expandBlobs();
            return;
        }
        if (allNotLoaded) {
            console.log("check loaded all NOT loaded");
            loading = true;
            
            //firefox will not see loaded file before it's played, Chrome not an issue
            buzz.all().load().mute().play();
        }
    };
    
    drumsSound.bind("loadeddata", function (e) {
        console.log("drums loaded");
        drumsLoaded = true;
        checkLoaded();
    });
    
    drumsSound.bind("error", function (e) {
        console.log("Drum load error: " + e);
    });
    
    bassSound.bind("loadeddata", function (e) {
        console.log("bass loaded");
        bassLoaded = true;
        checkLoaded();
    });
    
    bassSound.bind("error", function (e) {
        console.log("Bass load error: " + e);
    });
    
    leadSound.bind("loadeddata", function (e) {
        console.log("lead loaded");
        leadLoaded = true;
        checkLoaded();
    });
    
    leadSound.bind("error", function (e) {
        console.log("Lead load error: " + e);
    });
    
    breakSound.bind("loadeddata", function (e) {
        console.log("break loaded");
        breakLoaded = true;
        checkLoaded();
    });
    
    breakSound.bind("error", function (e) {
        console.log("Break load error: " + e);
    });
    
    //if all other sound are mute then restart audio with the first unmuted instrument
    var checkIfRestartRequired = function () {
        if (allSoundsInactive()) {
            buzz.all().setVolume(100);
            buzz.all().mute();
            buzz.all().stop();
            buzz.all().play();
        }
    };
    
    var drums = function () {
        checkIfRestartRequired();
        drumsActive = !drumsActive;
        setInstruments();
        setInstrumentPulse();
    };
    
    var bass = function () {
        checkIfRestartRequired();
        bassActive = !bassActive;
        setInstruments();
        setInstrumentPulse();
    };
    
    var lead = function () {
        checkIfRestartRequired();
        leadActive = !leadActive;
        setInstruments();
        setInstrumentPulse();
    };
    
    var breaks = function () {
        checkIfRestartRequired();
        breakActive = !breakActive;
        setInstruments();
        setInstrumentPulse();
    };
    
    $('#go-btn').bind('mousedown', function (evt) {
        if (blobsMoving || loading) {
            return;
        }
        
        goActive = !goActive;
        console.log("ACTIVE: " + goActive);
        
        if (goActive) {
            checkLoaded();
        } else {
            contractBlobs();
        }
    });
    
    //I was binding to mousedown and touchstart for touch devices but they both happened at the same time on touch devices
    
    $('#drums-btn').bind('mousedown', function (e) {
        if (!goActive) {
            return;
        }
        console.log("drum click");
        drums();
    });
    
    $('#bass-btn').bind('mousedown', function (e) {
        if (!goActive) {
            return;
        }
        console.log("bass click");
        bass();
    });
    
    $('#lead-btn').bind('mousedown', function (e) {
        if (!goActive) {
            return;
        }
        console.log("lead click");
        lead();
    });
    
    $('#break-btn').bind('mousedown', function (e) {
        if (!goActive) {
            return;
        }
        console.log("break click");
        breaks();
    });
    
}(window.MusicMaker = window.MusicMaker || {}, jQuery));