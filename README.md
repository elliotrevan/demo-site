# README #

### What is this repository for? ###

* Summary - This is a demo site for me to showcase my favourite web front end technologies
* Version - 1.0

### How do I get set up? ###

* Summary of set up
    * 'Node.js' and 'Node Package Manager' (npm) for dependencies  
    * 'Grunt' as the task manager used for:
        * conversion of scss files to css files
        * optimising images
        * concatenation of files (CSS and JS files)
        * minification/uglification of JS files
        * conversion of scss files to a concatinated css file
        * vendor autoprefing - no need for vendor prefix mixins in sass this is automatic
        * doing all this automatically when there are changes to files
    
* Configuration - All config is in the package.json and Gruntfile.js files
* Dependencies - [node 0.12.2, npm 2.7.4, ruby 2.1.5, gem 2.2.2, sass 3.4.13] all other dependecies are in the package.json file
* Database configuration - N/A
* How to run tests - TODO
* Deployment instructions - TODO - This should be a simple case of modifiying the html file to look at the minfied js file "concat.min.js" and the concatinated css file "global.css" and removing all sass and js folders and files.

### Contribution guidelines ###

* Writing tests - TODO
* Code review - TODO
* Other guidelines - TODO

### Who do I talk to? ###

* Repo owner or admin - Elliot Revan (elliotrevan@gmail.com)
* Other community or team contact - N/A