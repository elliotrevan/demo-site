module.exports = function (grunt) {
    'use strict';
    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        
        //check code quality
        jshint: {
            all: ['Gruntfile.js', 'js/*.js', '!js/xcolor.js']
        },
        
        jasmine : {
            // Your project's source files
            src : 'index.html',
            options: {
                specs : 'tests/specs/*spec.js',
                helpers : 'tests/specs/helpers/*.js',
                vendor: [
                    "http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js",
                    "http://d3js.org/d3.v3.min.js"
                ],
                keepRunner: true
            }
        },
    
        //concatinate files
        concat: {
            options: {
                sourceMap: true
            },
            js: {
                src:  'js/*.js', // All JS in the libs folder
                dest: 'js/concat.js'
            }
        },
        
        //minify files
        uglify: {
            options: {
                sourceMap: true
            },
            build: {
                src: 'js/concat.js',
                dest: 'js-min/concat.min.js'
            }
        },
        
        
        clean: {
            js: ['js/concat.js', 'js/concat.js.map']
        },
        
        //covert scss to css and concatenate
        sass: {
            options: {
                style: 'compressed',
//                    style: 'expanded',
                spawn: false,
                trace: true
            },
            dist: {
                files: {
                    'css/global.css': 'scss/all.scss'
//                    'css/main.css': 'scss/main.scss',
//                    'css/cube.css': 'scss/cube.scss'
                }
            }
        },
        
        autoprefixer: {
            single_file: {
                options: {
                    browsers: ['last 2 versions']
                },
                src: 'css/gobal.css'
            }
        },
        
        
        //optimise images
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'img/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'img-min/'
                }]
            }
        },
        
        watch: {
            options: {
                    //livereload: true
            },
            jschange: {
                files: ['js/*.js'],
                tasks: ['jshint', 'concat', 'uglify', 'clean'],
                options: {
                    spawn: false
                }
            },
            
            scss: {
                files: ['scss/modules/*.scss', 'scss/*.scss' ],
                tasks: ['sass', 'autoprefixer'],
                options: {
                    spawn: false
                }
            },
            
            imagechange: {
                files: ['img/*.*'],
                tasks: ['imagemin'],
                options: {
                    spawn: false
                }
            }
        }
    });

    // 3. Where we tell Grunt we plan to use the plug-ins.
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-autoprefixer');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['concat', 'uglify', 'clean', 'sass', 'jshint', 'watch']);
};